"""
Программа выполняет следующие действия:
-первая функция будет преобразовывать обрамлённое в звёздочки выражение в
теги <em> и </em>
-вторая функция будет преобразовывать обрамлённое в двойные звёздочки
выражение в теги <strong> и </strong>
"""
import argparse


def bold(usr_input: str) -> str:
    """
    Преобразовывает обрамлённое в звёздочки выражение в теги <em> и </em>
    :param usr_input: обрамленная в звездочки строка str
    :return: обрамленная в теги <em> и </em> строка str
    """
    usr_input = '<strong>' + usr_input[2:-2] + '</strong>'
    return usr_input


def itallic(usr_input: str) -> str:
    """
    Преобразовывает обрамлённое в двойные звёздочки выражение в теги <strong> и </strong>
    :param usr_input: обрамленная в двойные звёздочки строка str
    :return: обрамленная в теги <strong> и </strong> строка str
    """
    usr_input = '<em>' + usr_input[1:-1] + '</em>'
    return usr_input


def main():
    """
    Точка входа, реализация командного интерфейса.
    :return: None
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('string', type=str)
    args = parser.parse_args()

    if args.string[:2] == '**' and args.string[-2:] == '**':
        print(bold(args.string))
    elif args.string[:1] == '*' and args.string[-1:] == '*':
        print(itallic(args.string))
    elif args.string.found('*') == 0:
        print(args.string)
    else:
        print("Вы ввели некорректоное значение, попробуйте еще")


if __name__ == '__main__':
    main()